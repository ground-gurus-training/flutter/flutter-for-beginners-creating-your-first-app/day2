import 'package:flutter/material.dart';

final textStyle = TextStyle(
  fontSize: 32.0,
  fontWeight: FontWeight.bold,
);
