import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'utils/drawer_items.dart';
import 'views/about_page.dart';
import 'views/home_page.dart';
import 'views/settings_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int index = 0;
  final items = <Widget>[
    HomePage(),
    SettingsPage(),
    AboutPage(),
  ];

  void changePage(int idx) {
    setState(() {
      index = idx;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Navigation'),
        actions: [
          IconButton(
            onPressed: () => {},
            icon: FaIcon(FontAwesomeIcons.sun),
          ),
          IconButton(
            onPressed: () => {},
            icon: FaIcon(FontAwesomeIcons.moon),
          ),
        ],
      ),
      body: items[index],
      drawer: Drawer(child: drawerItems),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: index,
        onTap: changePage,
        items: const [
          BottomNavigationBarItem(
            icon: FaIcon(FontAwesomeIcons.house),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: FaIcon(FontAwesomeIcons.gear),
            label: 'Settings',
          ),
          BottomNavigationBarItem(
            icon: FaIcon(FontAwesomeIcons.person),
            label: 'About',
          ),
        ],
      ),
    );
  }
}
