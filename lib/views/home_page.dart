import 'package:flutter/material.dart';

import '../styles/font_style.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'Home',
        style: textStyle,
      ),
    );
  }
}
