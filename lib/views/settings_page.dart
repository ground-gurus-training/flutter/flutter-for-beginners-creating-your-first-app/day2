import 'package:flutter/material.dart';

import '../styles/font_style.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'Settings',
        style: textStyle,
      ),
    );
  }
}
