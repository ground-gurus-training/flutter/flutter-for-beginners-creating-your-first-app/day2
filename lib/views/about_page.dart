import 'package:flutter/material.dart';

import '../styles/font_style.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'About',
        style: textStyle,
      ),
    );
  }
}
