import 'package:flutter/material.dart';

import '../styles/font_style.dart';

final drawerItems = Padding(
  padding: const EdgeInsets.all(8.0),
  child: ListView(
    children: [
      Text('Hello', style: textStyle),
      Text('Hi', style: textStyle),
      Text('Howdy', style: textStyle),
    ],
  ),
);
